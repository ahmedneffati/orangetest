package com.example.ahmed.myapplication

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.post_item_layout.view.*

class PostItemAdapter(val postList: List<Post>, val context:  Context )  : RecyclerView.Adapter<PostItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):  ViewHolder {

        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.post_item_layout,
                parent, false))
    }

    override fun getItemCount(): Int {
        return postList.count()
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.txtPostTitle.text = postList.get(position).title
        holder.itemView.txtPostBody.text = postList.get(position).body


            Picasso.get().load("http://i.imgur.com/DvpvklR.png").into(holder.itemView.myImageView);

        holder.itemView.postD.isClickable = true
        holder.itemView.postD.setOnClickListener(View.OnClickListener {
            print("hello"+ holder.itemView.txtPostBody.text)
            val intent = Intent(context, Main2Activity::class.java)
            intent.putExtra("title", postList.get(position).title)
            intent.putExtra("desc", postList.get(position).body)
            intent.putExtra("imgUrl", "http://i.imgur.com/DvpvklR.png")
            context.startActivity(intent)
        })
        holder.itemView.postD.setOnLongClickListener{
            val colorValue = ContextCompat.getColor(context, R.color.colorAccent)


            holder.itemView.postD.setBackgroundColor(colorValue)
            return@setOnLongClickListener true
        }
        holder.itemView.btnShowAll.setOnClickListener(View.OnClickListener {
            holder.itemView.txtPostBody.maxLines = 100
        })
    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}